# pst_pw


## Main contributors

The main contributors (in alphabetic order) for this project are
 - Téo Guevit 
 - Anis Krari
 - Raphael Pignel-Dupont


## Coming up soon

- [x] set-up gitlab account
- [x] found data already extracted online
- [x] collect data from xfoil
- [x] implement data reader
- [ ] design a neural network
- [ ] implement training procedure
- [ ] add the data to the git
- [ ] implement a data-reader


## How to setup

You will need a few things. First, cloen this repository, using something like `git clone git@gitlab.com:ey_datakalab/pst_pw.git`. 

## How to install

Once you went through the set-up, you can install the required python packages. In a conda environment, you can simply run `python -m src.plane_wing` and it will give you the pip commands to use. Follow these steps:
 1. run `conda create -n pst python=3.9`
 2. run `conda activate pst`
 3. run `python -m src.plane_wing` and follow the instructions from there

Note that these instructions won't work on a M1/M2 mac check [this link](https://caffeinedev.medium.com/how-to-install-tensorflow-on-m1-mac-8e9b91d93706).
