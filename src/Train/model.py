import tensorflow as tf


def custom_activation(x):
    return 2 * tf.nn.tanh(x)


def create_model(num_angles: int) -> tf.keras.Model:

    inputs = tf.keras.layers.Input(
        shape=(num_angles, 5))  # shape = (None, 10, 5)

    x = tf.keras.layers.BatchNormalization(axis=1)(inputs)

    x = tf.keras.layers.Flatten()(x)  # shape = (None, 50)

    x = tf.keras.layers.Dense(32)(x)  # shape = (None, 32)
    x = tf.keras.layers.Activation("relu")(x)  # shape = (None, 32)
    x = tf.keras.layers.Dropout(0.2)(x)

    x = tf.keras.layers.Dense(32)(x)  # shape = (None, 32)
    x = tf.keras.layers.Activation("relu")(x)  # shape = (None, 32)
    x = tf.keras.layers.Dropout(0.2)(x)

    x = tf.keras.layers.Dense(units=num_angles * 2)(x)  # shape = (None, 20)

    outputs = tf.keras.layers.Reshape(target_shape=(
        num_angles, 2))(x)  # shape = (None, 10, 2)

    model = tf.keras.Model(inputs, outputs)

    return model
