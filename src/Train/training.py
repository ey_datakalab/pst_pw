from .model import create_model
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt


def train_a_model(training_set: tf.data.Dataset, num_angles: int) -> None:
    """
    trains a neural network

    Args:
        training_set: data iterator
        num_angles: defiens the shape of the inputs
        ???
    """
    model = create_model(num_angles=num_angles)  # to complete
    model.summary()
    loss_function = (
        tf.keras.losses.MeanSquaredError()
    )  # to complete -> mean squared error ?
    # Adam plutot que SGD car converge plus vite
    optimizer = tf.keras.optimizers.Adam()
    model.compile(
        metrics=["mse"]
    )

    @tf.function
    def optimization_step(images: tf.Tensor, labels: tf.Tensor) -> tf.Tensor:
        """
        runs an optimization step

        Args:
            images: inputs to use
            labels: ground truth
        """
        with tf.GradientTape() as tape:
            tape.watch(model.trainable_variables)
            predictions = model(images, training=False)

            loss = loss_function(y_true=labels, y_pred=predictions)

            gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        return loss

    y = np.array([])
    for cpt, (inputs, labels) in enumerate(training_set):
        # print("inputs shape (batch-size, num_angles, num of infos):", inputs.shape)
        # print("labels shape (batch-size, num_angles, num of infos):", labels.shape)
        # print(inputs[1])  # à enlever
        # print(labels[1])  # à enlever
        loss_value = optimization_step(images=inputs, labels=labels)
        print(loss_value.numpy())
        y = np.append(y, loss_value)
        if cpt == 5000:
            break

    x = np.arange(1, len(y) + 1)
    plt.plot(x, y)
    plt.ylabel('loss_value')
    plt.show()

    model.save("trained_model.h5")

    data = [[100000, 0, 0, 12, 0],
            [100000, 0, 0, 12, 1],
            [100000, 0, 0, 12, 2],
            [100000, 0, 0, 12, 3],
            [100000, 0, 0, 12, 4],
            [100000, 0, 0, 12, 5],
            [100000, 0, 0, 12, 6],
            [100000, 0, 0, 12, 7],
            [100000, 0, 0, 12, 8],
            [100000, 0, 0, 12, 9]]

    x = np.array(data)
    x = x.reshape((1, 10, 5))

    predictions = model.predict(x)
    print(predictions)
