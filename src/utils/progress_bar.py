from typing import Any
import os
import time


color_map = {
    "cyan":"\033[96m",
    "blue": "\033[94m",
    "purple": "\033[95m",
    "green": "\033[92m",
    "orange": "\033[93m",
    "red": "\033[91m",
}

class FancyProgressBar:
    def __init__(
        self,
        total_steps: int,
        base_str: str = "Fine-Tuning",
        standard_shell: bool = True,
        color:str="cyan"
    ) -> None:
        self.total_steps = total_steps
        self.base_str = base_str
        self.standard_shell = standard_shell
        self.progress_bar_size = self.get_size() - (36 - (21 - len(self.base_str)))
        self.current_progression = 0
        self.color=color
        self.start_time = time.time()

    def get_size(self):
        if self.standard_shell:
            return os.get_terminal_size().columns
        else:
            return 60

    def get_remaining_time(self) -> str:
        one_step_duration = (time.time() - self.start_time) / (
            self.current_progression + 1
        )
        remaining_steps = self.total_steps - (self.current_progression + 1)
        remaining_time = one_step_duration * remaining_steps
        return time.strftime("%H:%M:%S", time.gmtime(remaining_time))

    def runtime(self) -> str:
        duration = time.time() - self.start_time
        return time.strftime("%H:%M:%S", time.gmtime(duration))

    def __call__(
        self, progression_complete: bool = False, *args: Any, **kwds: Any
    ) -> Any:
        if self.get_size() - (36 - (21 - len(self.base_str))) != self.progress_bar_size:
            print("\r", end="")
            self.progress_bar_size = self.get_size() - (36 - (21 - len(self.base_str)))
        base_string = f"[{color_map[self.color]}{self.base_str}\033[0m]"
        if progression_complete:
            bar = f"[{color_map[self.color]}" + "=" * self.progress_bar_size + "\033[0m]"
            print(f"\r{base_string} {bar} {self.runtime()}")
        else:
            percentage = int(
                self.progress_bar_size * self.current_progression / self.total_steps
            )
            bar = (
                f"[{color_map[self.color]}"
                + "=" * percentage
                + " " * (self.progress_bar_size - percentage)
                + "\033[0m]"
            )
            self.current_progression += 1
            print(f"\r{base_string} {bar} {self.get_remaining_time()}", end="")
