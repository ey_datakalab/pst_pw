from ..utils.progress_bar import FancyProgressBar
import numpy as np
import re
from typing import List
import csv
import os


def read_data(data_path: str) -> List[np.ndarray]:
    """
    collects all data from a single folder containing
    all csv files

    Args:
        data_path: path to csv folder
    """
    files = [os.path.join(data_path, e)
             for e in os.listdir(data_path) if ".csv" in e]
    raw_data = []
    pbar = FancyProgressBar(
        total_steps=len(files), base_str="ExtractData", color="blue"
    )
    for file in files:
        pbar()
        arr_current_file = []
        start_recording = False
        with open(file, newline="") as csvfile:
            spamreader = csv.reader(csvfile, delimiter=",", quotechar="|")
            for row in spamreader:
                if start_recording:
                    arr_current_file.append(
                        [foil1, foil2, foil34, reynolds] + [float(e) for e in row])
                else:
                    if len(row) == 2:
                        if row[0] == "Reynolds number":
                            reynolds = float(row[1]) / 100000
                        elif row[0] == "Airfoil":
                            foil1 = float(re.findall(r"\d+", row[1])[0][0])
                            foil2 = float(re.findall(r"\d+", row[1])[0][1])
                            foil34 = float(re.findall(r"\d+", row[1])[0][2])
                if len(row) == 7 and row[0] == "Alpha":
                    start_recording = True
        raw_data.append(np.array(arr_current_file))
    pbar(progression_complete=True)
    return raw_data
