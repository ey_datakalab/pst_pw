import tensorflow as tf
from typing import List, Tuple
import numpy as np


def create_training_data_iterator(
    raw_data: List[np.ndarray], batch_size: int = 4, num_angles: int = 10
) -> tf.data.Dataset:
    """
    creates a data.Dataset for tensorflow training
    This behaves like list or a python iterator, i.e. you can use it in a for loop
    The advantage over a simpel list is its use of the gpu/cpu

    Args:
        raw_data: list of numpy arrays of raw data from which we will define
            our training examples
        batch_size: training btach size (size of a batch of examples for each training step)
        num_angles: number of angles to consider to define one data point
    """
    slices = []
    i = 0
    for cpt in range(len(raw_data)):
        last_i = i
        for cpt_ in range(len(raw_data[cpt]) - num_angles):
            slices.append(i)
            i += 1
        i = last_i + len(raw_data)
    dataset = tf.data.Dataset.from_tensor_slices(slices)
    dataset = dataset.shuffle(len(slices))
    dataset = dataset.repeat()
    tf_raw_data = tf.constant(np.concatenate(raw_data, axis=0))

    def load_batch(time_stamp: tf.Tensor) -> Tuple[tf.Tensor, tf.Tensor]:
        time_stamps = tf.constant(value=list(range(num_angles))) + time_stamp
        rows = tf.gather(tf_raw_data, time_stamps)

        eps = 1e-8  # une petite constante positive

        x = tf.gather(rows, [0, 1, 2, 3, 4], axis=-1)
        y = tf.gather(rows, [5, 6], axis=-1)

        return x, y

    dataset = dataset.map(
        load_batch, num_parallel_calls=tf.data.AUTOTUNE, deterministic=False
    )
    dataset = dataset.batch(batch_size)
    return dataset
