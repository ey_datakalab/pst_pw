"""
This project studies the inlfuence of a wing shape on its flying properties
in the context of a PST at Centrale-Supelec.
"""


if __name__ == "__main__":
    import os

    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    import argparse

    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "--logger",
        nargs="?",
        type=str,
        default="logs.log",
        help="path to save the logs (if you use the default value the file will be overwritten)",
    )
    parser.add_argument(
        "--path",
        nargs="?",
        type=str,
        default="raw_data",
        help="path to the raw data",
    )
    parser.add_argument(
        "--batch_size",
        nargs="?",
        type=int,
        default=4,
        help="number of examples to define a training batch",
    )
    parser.add_argument(
        "--num_angles",
        nargs="?",
        type=int,
        default=10,
        help="number of angles to consider to define one data point",
    )
    args = parser.parse_args()
    from .utils.GetLogger import get_logger
    from .utils.Clear import remove_chache_folders
    from .utils.Packages import check_packages

    logger = get_logger(log_file=args.logger)
    check_packages(logger=logger)
import logging
from .Data.reader import read_data
from .Data.iterator import create_training_data_iterator
from .Train.model import create_model
#from .Train.training import train_a_model
import numpy as np
import tensorflow as tf


# import tensorflow as tf


def test_a_neural_netwrok(
    data_path: str, batch_size: int, logger: logging.Logger, num_angles: int
) -> None:
    """
    This function will train a neural network to solve our task

    Args:
        data_path: path to the raw data
        batch_size: number of examples to define a training batch
        logger: logger for verbose recording
        num_angles: number of angles to consider to define one data point
    """
    raw_data = read_data(data_path=data_path)
    train_iterator = create_training_data_iterator(
        raw_data=raw_data, batch_size=batch_size, num_angles=num_angles
    )

    data = [[100000, 0, 0, 12, 0],
            [100000, 0, 0, 12, 1],
            [100000, 0, 0, 12, 2],
            [100000, 0, 0, 12, 3],
            [100000, 0, 0, 12, 4],
            [100000, 0, 0, 12, 5],
            [100000, 0, 0, 12, 6],
            [100000, 0, 0, 12, 7],
            [100000, 0, 0, 12, 8],
            [100000, 0, 0, 12, 9]]

    x = np.array(data)
    x = x.reshape((1, 10, 5))

    model = tf.keras.models.load_model('trained_model.h5')

    predictions = model.predict(x)
    print(predictions)

    #train_a_model(training_set=train_iterator, num_angles=num_angles)

    # data_de_test=read_data(data_path="test_data")
    # test_iterator = create_training_data_iterator(
    #     raw_data=data_de_test, batch_size=batch_size, num_angles=num_angles
    # )
    # model_de_test = tf.keras.models.load_model("trained_model.h5")
    # model_de_test.compile(
    #     metrics=["mse"]
    # )
    # model_de_test.evaluate(test_iterator)


if __name__ == "__main__":
    test_a_neural_netwrok(
        data_path=args.path,
        batch_size=args.batch_size,
        logger=logger,
        num_angles=args.num_angles,
    )
    remove_chache_folders()
